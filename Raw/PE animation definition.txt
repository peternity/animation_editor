Framedata tree is the following:
- Animation ID # (In order, first one is #0, etc)
  - "directions" in the following order: (RIGHT, UP_RIGHT, UP, UP_LEFT, LEFT, DOWN_LEFT, DOWN, DOWN_RIGHT)
       hitPoint: duration at which frame should 'hit' another (not used)
       returnPoint: duration at which the Pokemon returns back to his (not used) 
       rushPoint: duration at which Pokemon leaps forward (not used)
       totalDuration - Total # of duration of all animations

     - "frames" of the "direction"
       frameIndex - frame sprite # as shown on the spreadsheet
       duration - how many frames this lasts
       flip - flip current frame
       offset - [x, y] move frame on x or y 
       shadowOffset : [x, y] move shadow frame on x or y

Animations:
    # Internal
    SPATK = 7
    DEAD = 3

    STOP = 0
    IDLE = 1
    WALK = 2
    SLEEP = 3
    HURT = 4
    ATK = 5
    CHARGE = 6
    SHOOT = 7
    STRIKE = 8
    CHOP = 9
    SCRATCH = 10
    PUNCH = 11
    SLAP = 12
    SLICE = 13
    MULTI_SCRATCH = 14
    MULTI_STRIKE = 15
    UPPERCUT = 16
    RICOCHET = 17
    BITE = 18
    SHAKE = 19
    JAB = 20
    KICK = 21
    LICK = 22
    SLAM = 23
    STOMP = 24
    APPEAL = 25
    DANCE = 26
    TWIRL = 27
    TAILWHIP = 28
    SING = 29
    SOUND = 30
    RUMBLE = 31
    FLAP_AROUND = 32
    GAS = 33
    SHOCK = 34
    EMIT = 35
    SPECIAL = 36
    WITHDRAW = 37
    REAR_UP = 38
    SWELL = 39
    SWING = 40
    DOUBLE = 41
    ROTATE = 42
    SPIN = 43
    JUMP = 44
    HIGH_JUMP = 45